#EXTM3U
#EXT-X-VERSION:3
#EXT-X-TARGETDURATION:${(max_duration/10000000)+1}
#EXT-X-MEDIA-SEQUENCE:0
#EXT-X-PLAYLIST-TYPE:VOD
% for s in segs:
#EXTINF:${"%0.3f" % (s['duration']/10000000.0)},
${s['url']}
% endfor
#EXT-X-ENDLIST
